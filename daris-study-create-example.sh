#! /bin/bash
# creates a new study and outputs the daris id of the study so pipe it to a file to be used later if you like
# $1 is your daris domain
# $2 is your daris username

# $3 is the name you want to give the study eg BrainSegmentation
# $4 is the original daris object for which this processed study is based
# $5 is daris identifier of the root object eg 1.7.56.11.1

mkdir temp

pw=$(zenity --password)

#need to get meta data from original data
daris-download --mf.transport https --mf.host daris.researchsoftware.unimelb.edu.au --mf.port 443 --mf.auth $1,$2,$pw --no-descendants --parts meta --output-dir temp $4 

metaxml=$(find temp -name $4.daris.metadata.xml)

echo $metaxml

step=$(grep -oE '<step>[^<]*</step>' $metaxml | sed -e 's/<step>//g' |sed -e 's/[</step>]//g')




echo daris-study-create --mf.transport https --mf.host daris.researchsoftware.unimelb.edu.au --mf.port 443 --mf.auth $1,$2,$pw --pid $5 --name $3 --step $step


#clean up

rm -rf temp

