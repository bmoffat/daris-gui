#! /bin/bash

# $1 is your daris domain
# $2 is your daris username
# $3 is your daris password
# $4 is the nii or nii.gz or directory containing nii files eg BrainSegmentationResampled.nii.gz
# $5 is daris identifier 1.7.56.11.1.1

daris-dataset-upload --mf.transport https --mf.host daris.researchsoftware.unimelb.edu.au --mf.port 443 --mf.auth $1,$2,$3 --pid $5 --input $4 --type nifti/series --derived true



