#! /bin/bash

# Simple zenity launcher of daris upload gui

ddata=$(zenity --forms --title="Download a study from DARIS" \
	--text="Data will be in folder called temp" \
	--separator="," \
	--add-entry="Domain" \
	--add-entry="User Name" \
	--add-password="password" \
	--add-entry="Daris PID of data" \
	#--add-entry="Daris PID of Subject" \
	
	#--add-entry="Name of Study"
 	)
accepted=$?
#	--add-calendar="Birthday" >> addr.csv

#echo $ddata
#echo $accepted
#: '
case $accepted in
    0)
        #echo $ddata
	#echo $ddata | awk -F, '{ for (i = 1; i < NF; ++i ) print $i }'
	domain=$(echo $ddata | awk -F, '{ print $1 }')
	username=$(echo $ddata | awk -F, '{ print $2 }')
	password=$(echo $ddata | awk -F, '{ print $3 }')	
	pid_orig=$(echo $ddata | awk -F, '{ print $4 }')
	#pid_sub=$(echo $ddata | awk -F, '{ print $4 }')
	
	#sname=$(echo $ddata | awk -F, '{ print $6 }')
	#domain=$(awk -F 'print $1' $ddata)
	echo $domain

mkdir -p temp



	#need to get meta data from original data
	daris-download --mf.transport https --mf.host daris.researchsoftware.unimelb.edu.au --mf.port 443 --mf.auth $domain,$username,$password --no-descendants --parts content --output-dir temp $pid_orig

	
	#clean up

	#rm -rf temp


	;;
    1)
        echo "Cancelled"
	;;
    -1)
        echo "An unexpected error has occurred."
	;;
esac

#'
