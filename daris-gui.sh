#! /bin/bash

# Simple zenity launcher of daris upload gui

ddata=$(zenity --forms --title="Upload a new study to DARIS" \
	--text="Enter information " \
	--separator="," \
	--add-entry="Domain" \
	--add-entry="User Name" \
	--add-entry="Daris PID of Original data" \
	--add-entry="Daris PID of Subject" \
	--add-password="password" \
	--add-entry="Name of Study"
 	)
accepted=$?
#	--add-calendar="Birthday" >> addr.csv

#echo $ddata
#echo $accepted
#: '
case $accepted in
    0)
        #echo $ddata
	#echo $ddata | awk -F, '{ for (i = 1; i < NF; ++i ) print $i }'
	domain=$(echo $ddata | awk -F, '{ print $1 }')
	username=$(echo $ddata | awk -F, '{ print $2 }')
	pid_orig=$(echo $ddata | awk -F, '{ print $3 }')
	pid_sub=$(echo $ddata | awk -F, '{ print $4 }')
	password=$(echo $ddata | awk -F, '{ print $5 }')
	sname=$(echo $ddata | awk -F, '{ print $6 }')
	#domain=$(awk -F 'print $1' $ddata)
	echo $domain

mkdir temp



	#need to get meta data from original data
	daris-download --mf.transport https --mf.host daris.researchsoftware.unimelb.edu.au --mf.port 443 --mf.auth $domain,$username,$password --no-descendants --parts meta --output-dir temp $pid_orig

	metaxml=$(find temp -name $pid_orig.daris.metadata.xml)

	echo $metaxml

	step=$(grep -oE '<step>[^<]*</step>' $metaxml | sed -e 's/<step>//g' |sed -e 's/[</step>]//g')

	daris-study-create --mf.transport https --mf.host daris.researchsoftware.unimelb.edu.au --mf.port 443 --mf.auth $domain,$username,$password --pid $pid_sub --name $sname --step $step


	#clean up

	rm -rf temp


	;;
    1)
        echo "Cancelled"
	;;
    -1)
        echo "An unexpected error has occurred."
	;;
esac

#'
