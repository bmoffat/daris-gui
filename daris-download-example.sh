#! /bin/bash

# $1 is your daris domain
# $2 is your daris username
# $3 is your daris password
# $4 is the output directory path eg <~/Downloads>
# $5 is daris identifier eg 1.7.56.11.1.1.8


daris-download --mf.transport https --mf.host daris.researchsoftware.unimelb.edu.au --mf.port 443 --mf.auth $1,$2,$3 --output-dir $4 $5
